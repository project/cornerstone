/* eslint strict: ["error", "global"] */
/* eslint-disable no-console, indent */
const { task, series } = require('gulp');

// Gulp tasks.
const env = require('./tasks/env');
const config = require('./tasks/config');
const watchTask = require('./tasks/watch');
const buildTask = require('./tasks/build');
const browserSyncTask = require('./tasks/browsersync');

// Show what the current environment is set to.
console.log(
  env === 'prod' ? 'CURRENT ENVIRONMENT: prod' : 'CURRENT ENVIRONMENT: dev'
);

// Build all theme assets.
task('build', series(buildTask));

// Run watch task by default.
task(
  'default',
  series(
    'build',
    config.browsersync.enabled === true
      ? browserSyncTask.serve
      : done => done(),
    watchTask
  )
);
