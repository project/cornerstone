SUBTHEME_MACHINE_NAME.retina:
  label: Retina Image
  mediaQuery: ''
  weight: 0
  multipliers:
    - 1x
    - 2x
