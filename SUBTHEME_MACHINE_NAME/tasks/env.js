module.exports = (function() {
  'use strict';

  const argv = require('minimist')(process.argv.slice(2));

  return argv.env === 'prod' ? argv.env : 'dev';
})();
