'use strict';

const { series, parallel } = require('gulp');
const config = require('./config');

const cleanTask = require('./clean');
const modernizrTask = require('./modernizr');
const fontsTask = require('./fonts');
const imagesTask = require('./images');
const jsTask = require('./scripts');
const sassTask = require('./styles');

module.exports = series(
  cleanTask,
  parallel(imagesTask, fontsTask),
  sassTask,
  config.modernizr.enabled ? modernizrTask : done => done(),
  jsTask
);
