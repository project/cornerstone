module.exports = function modernizr() {
  'use strict';

  const config = require('./config');
  const { src, dest } = require('gulp');
  const modernizr = require('gulp-modernizr');
  const terser = require('gulp-terser');

  let stream = src(config.modernizr.src);

  if (config.modernizr.enabled) {
    stream = stream
      .pipe(
        modernizr('modernizr-custom.js', {
          tests: ['details', 'touchevents', 'inputtypes'],
          options: ['addTest', 'prefixes', 'setClasses', 'testStyles'],
        })
      )
      .pipe(terser())
      .pipe(dest(config.modernizr.dest));
  }

  return Promise.resolve(stream);
};
