module.exports = (function config() {
  'use strict';

  // Pull the browserlist settings from the package.json file so we only have
  // to set them in one place.
  const packageConfig = require('../package.json');
  const gulpConfig = require('../gulp.config');
  const env = require('./env');

  function browserSyncConfig() {
    'use strict';

    // By default disable browser-sync unless it's enabled in the config file.
    let bsConfig = {
      enabled: false,
    };

    try {
      // You must rename and configure the example .bs-config.example.js
      // browsersync config file.
      bsConfig = require('../.bs-config');
    } catch (ex) {}

    return bsConfig;
  }

  let config = gulpConfig;
  config.browsersync = browserSyncConfig();
  config.postcss.autoprefixer = packageConfig.browserslist;

  if (env === 'prod') {
    config.postcss.cssnano.enabled = true;
    config.drupal.enabled = false;
    config.sourcemaps.enabled = false;
    config.notify.enabled = false;
    config.browsersync.enabled = false;
  }

  return config;
})();
