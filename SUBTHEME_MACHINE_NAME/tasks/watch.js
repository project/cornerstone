module.exports = function watch() {
  'use strict';

  const config = require('./config');
  const { watch, series } = require('gulp');
  const browserSyncTask = require('./browsersync');
  const fontsTask = require('./fonts');
  const imagesTask = require('./images');
  const jsTask = require('./scripts');
  const sassTask = require('./styles');

  if (config.browsersync.enabled === true) {
    watch(config.sass.src, series(sassTask, browserSyncTask.stream));
    watch(config.images.src, series(imagesTask, browserSyncTask.reload));
    watch(config.js.src, series(jsTask, browserSyncTask.reload));
    watch(config.fonts.src, series(fontsTask, browserSyncTask.reload));
    if (config.drupal.enabled) {
      watch(config.drupal.src, series(browserSyncTask.reload));
    }
  } else {
    watch(config.sass.src, series(sassTask));
    watch(config.images.src, series(imagesTask));
    watch(config.js.src, series(jsTask));
    watch(config.fonts.src, series(fontsTask));
  }
};
