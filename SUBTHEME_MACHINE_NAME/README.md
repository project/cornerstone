# Instructions

The Drupal subtheme based on the Cornerstone base theme requires several npm modules to be installed to generate front-end assets. You can install these by running the command `npm install` in the subtheme directory.

## Dependencies

The following dependencies must be installed to run the front-end build tasks:

1. [NodeJS](https://nodejs.org)
1. [GulpJS CLI](https://gulpjs.com)

## Sass libraries

The following Sass libraries are installed and imported by default:

- [include-media](https://include-media.com)
  - Media query breakpoint management.
- [Bourbon v4](http://bourbon.io)
  - Sass Mixin library.
- [Modularscale](https://github.com/modularscale/modularscale-sass)
  - Modular scale mixin/function library.
- [sass-yiq](https://github.com/timhettler/sass-yiq)
  - Sass YIQ color contrast function.
- [font-face-generator](https://www.npmjs.com/package/font-face-generator)
  - Webfont @font-face Sass generator.
- [Select Box](https://github.com/dbox/select-box)
  - Helper mixin for styling select form elements.

### Optional libraries

The following optional Sass libraries are installed, but not imported by default:

- [Family.scss](https://lukyvj.github.io/family.scss)
  - Quantity media query library.
- [range-slider-sass](https://github.com/Jimdo/range-slider-sass)
  - Helper mixin for styling range form elements.

These can be imported by uncommenting their import statements in the `src/sass/_partials/1-tools/_import.tools.scss` file.

## Starting Front-End Build Task

After the dependencies have been installed, you can run the [GulpJS](https://gulpjs.com) build/watch task by running the command `gulp` in the subtheme directory. All of the Gulp tasks are defined in the `gulpfile.js` and `gulp.config.js` files.

Additionally, some linter configs are included such as:

1. [EditorConfig](https://editorconfig.org)
1. [ESlint](https://eslint.org)
1. [StyleLint](https://stylelint.io)
1. [Prettier](https://prettier.io)

## Theme Sass structure

The theme's Sass structure largely follows the [SMACSS](https://smacss.com) methodologyand uses the following folder structure.

1. Tools: Vendor libraries and custom Sass functions, mixins, and variables. Nothing in the core import generates CSS by default.
1. Vendor: Third party Sass and CSS libraries that generate CSS by default.
1. Base: Reset and/or normalize styles, global style definitions, and unclassed HTML elements (e.g. `h2`, `ul`) styling.
1. Layouts: Abstract layout and main site layout sections. Should primarily be used for object/positioning. Minimal to no styling.
1. Components: Reuseable styled UI components.
1. Utilities: Helper/utility classes. Use sparingly as many are hard overrides using !important.
1. Hotfix: Emergency styling fixes. This file should be regularly refactored into new components/objects, or replaced with existing ones.

Print specific versions of this structure are located in the `_print` partials folder.

## Grid System

A flexbox bases grid system is also included with the theme.
