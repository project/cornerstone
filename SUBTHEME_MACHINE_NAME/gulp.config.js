'use strict';

const gulpConfig = {
  clean: {
    enabled: true,
    paths: ['assets/**', '!assets'],
  },
  postcss: {
    enabled: true,
    // The browserlist settings used by autoprefixer will be automatically
    // loaded from package.json
    autoprefixer: {},
    assets: {
      relative: true,
      loadPaths: 'assets',
    },
    easysprite: {
      imagePath: './assets/images',
      spritePath: './assets/images/sprites',
    },
    easingGradients: {
      colorStops: 15,
      alphaDecimals: 5,
      colorMode: 'lrgb',
    },
    cssnano: {
      enabled: false, // This will be automatically enabled in 'prod' mode.
      preset: ['default'],
    },
  },
  sass: {
    enabled: true,
    settings: {
      outputStyle: 'expanded',
      precision: 10,
      sourceMap: true,
    },
    src: 'src/sass/**/*.scss',
    dest: 'assets/css/',
    sourcemaps: '_sourcemaps',
  },
  js: {
    enabled: true,
    src: ['src/js/**/*.js', '!src/js/**/_drupal_js_template.js'],
    dest: 'assets/js/',
    sourcemaps: '_sourcemaps',
  },
  babel: {
    enabled: true,
    presets: ['@babel/env'],
  },
  fonts: {
    enabled: true,
    src: 'src/fonts/',
    dest: 'assets/fonts/',
  },
  images: {
    enabled: true,
    src: 'src/images/',
    dest: 'assets/images/',
  },
  drupal: {
    enabled: false,
    src: '**/*.{twig,theme,yml}',
  },
  modernizr: {
    enabled: false,
    src: 'assets/css/**/*.css',
    dest: 'assets/js/modernizr/',
  },
  sourcemaps: {
    enabled: true,
  },
  notify: {
    enabled: true,
  },
};

module.exports = gulpConfig;
