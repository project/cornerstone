/**
 * @file
 * A Drupal JS Boilerplate template file.
 */
/* eslint-disable no-unused-vars */

(function(Drupal) {
  'use strict';

  Drupal.behaviors.TEMPLATE_FILE = {
    attach: function(context, settings) {},
  };
})(Drupal);
