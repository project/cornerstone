# Linters

Configuration files are included for the following linters. Combined with suggested [VSCode extensions](/editor-support/vscode.md) these allow for automatic formatting of code when saving.

| Language | Linter(s) | Config File(s) | Ignore Override File |
| --- | --- | --- | --- |
| _General Text File Formats_ | [EditorConfig](https://editorconfig.org) | `.editorconfig` | None |
| `Sass` | [Stylelint](https://stylelint.io) | `.stylelintrc.json` | `.stylelintignore` |
| `JavaScript` | [ESLint](https://eslint.org) + [Prettier](https://prettier.io) | `.eslintrc.json` + `.prettierrc.js` | `.eslintignore` + `.prettierignore` |
| `Markdown` | [markdownlint](https://github.com/DavidAnson/markdownlint) + [Prettier](https://prettier.io) | `.markdownlint.json` + `.prettierrc.js` | _Property in_ `.markdownlint.json` + `.prettierignore` |
