# VSCode

While Cornerstone can be used with any code editor, some built-in support is provided for [VSCode](https://code.visualstudio.com). This includes workspace default settings which make setting up VSCode for Drupal theming and module development easier. It also includes workspace extension suggestions for Drupal development and linting of Sass, JavaScript, and other files. All settings are stored in the hidden `.vscode` folder.

## Extension Suggestions

| Extension Name | Description |
| --- | --- |
| [Apache Conf (mrmlnc.vscode-apache)](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-apache) | Syntax highlighter for Apache configuration files. |
| [Cucumber (Gherkin) Full Support (stevejpurves.cucumber)](https://marketplace.visualstudio.com/items?itemName=stevejpurves.cucumber) | VSCode Cucumber (Gherkin) Full Language Support + Formatting + Autocomplete. |
| [DotENV (mikestead.dotenv)](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) | Support for dotenv file syntax. |
| [Drupal 8 JavaScript Snippets (tsega.drupal-8-javascript-snippets)](https://marketplace.visualstudio.com/items?itemName=tsega.drupal-8-javascript-snippets) | Useful Javascript snippets to use with Drupal 8. |
| [Drupal 8 Snippets (dssiqueira.drupal-8-snippets)](https://marketplace.visualstudio.com/items?itemName=dssiqueira.drupal-8-snippets) | Lint-on-save with ESLint. |
| [Drupal 8 Twig Snippets (tsega.drupal-8-twig-snippets)](https://marketplace.visualstudio.com/items?itemName=tsega.drupal-8-twig-snippets) | A handful of twig functions to use with Drupal 8. |
| [Drupal Syntax Highlighting (marcostazi.vs-code-drupal)](https://marketplace.visualstudio.com/items?itemName=marcostazi.VS-code-drupal) | Provides syntax highlighting support for Drupal specific file types, such as .module, .inc, .theme etc. |
| [EditorConfig for VS Code (EditorConfig.EditorConfig)](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) | EditorConfig Support for Visual Studio Code. |
| [ESLint (dbaeumer.vscode-eslint)](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) | Integrates ESLint JavaScript into VS Code. |
| [Gherkin Indent (aravindkumar.gherkin-indent)](https://marketplace.visualstudio.com/items?itemName=AravindKumar.gherkin-indent) | Gherkin Indent sets the indentation for Gherkin (cucumberjs) steps and examples. Automatically applies coloring for features, scenarios and steps in .feature files. |
| [markdownlint (davidanson.vscode-markdownlint)](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) | Markdown linting and style checking for Visual Studio Code. |
| [npm (eg2.vscode-npm-script)](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script) | npm support for VS Code. |
| [PHP Debug (felixfbecker.php-debug)](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) | Debug support for PHP with XDebug. |
| [PHP DocBlocker (neilbrayfield.php-docblocker)](https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker) | A simple, dependency free PHP specific DocBlocking package . |
| [PHP Intelephense (bmewburn.vscode-intelephense-client)](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client) | High performance, feature rich PHP intellisense. |
| [phpcbf (valeryanm.vscode-phpcbf)](https://marketplace.visualstudio.com/items?itemName=ValeryanM.vscode-phpcbf) | PHP Code Beautifier & Fixer for Visual StudioCode. |
| [phpcs (ikappas.phpcs)](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs) | PHP CodeSniffer for Visual Studio Code. |
| [Prettier - Code formatter (esbenp.prettier-vscode)](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) | VS Code plugin for prettier/prettier. |
| [SCSS IntelliSense (mrmlnc.vscode-scss)](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-scss) | Advanced autocompletion and refactoring support for SCSS. |
| [stylelint (shinnn.stylelint)](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint) | Modern CSS/SCSS/Less linter. |
| [Twig (whatwedo.twig)](https://marketplace.visualstudio.com/items?itemName=whatwedo.twig) | Syntax highlighting for PHP Twig. |
| [Twigcs Linter (cerzat43.twigcs)](https://marketplace.visualstudio.com/items?itemName=cerzat43.twigcs) | Twig CodeSniffer for Visual Studio Code. |
| [Vagrantfile Support (marcostazi.vs-code-vagrantfile)](https://marketplace.visualstudio.com/items?itemName=marcostazi.VS-code-vagrantfile) | Provides syntax highlighting support for Vagrantfile, useful if you use Vagrant. |
| [YAML (redhat.vscode-yaml)](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) | YAML Language Support by Red Hat, with built-in Kubernetes and Kedge syntax support. |
