# General Requirements

- A [Drupal 8](https://www.drupal.org/8) installation.
- [Node.js](https://nodejs.org/en/) version 10.x (only for sub-theme generator option)

## Base Theme Requirements

Installing Cornerstone has no additional technical requirements beyond what Drupal requires itself.

## Sub-theme Requirements

To create a sub-theme using the built in sub-theme generator CLI [option #1](/getting-started/create-sub-theme.md#option-1-sub-theme-generator), you must have [Node.js](https://nodejs.org/en/) and [NPM](https://www.npmjs.com/) (now packaged with Node.js) installed.

You can create a sub-theme from Cornerstone using [option #2](/getting-started/create-sub-theme.md#option-2-manual-string-replacement) without additional dependencies.
