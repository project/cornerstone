# Installing Theme

> [!DANGER] Currently the Cornerstone theme is not hosted on [Drupal.org](https://drupal.org). Because of this you will not be able to install the theme using the `drush dl` command.

## Option #1: Composer

Cornerstone is available as a composer package, and can be easily installed in a composer based project using the command:

```shell
composer require fls/cornerstone_theme
```

A composer based workflow is highly recommended. It makes updating and managing any dependencies for the Cornerstone theme and Drupal itself much easier.

## Option #2: Manual Installation

If a composer based workflow is not possible, the theme can also be installed manually.

1. Go to [https://bitbucket.org/figleafsoftware/cornerstone-theme/downloads](https://bitbucket.org/figleafsoftware/cornerstone-theme/downloads) and download the theme's git repo as a zip file.

2. Extract the zip file and copy its contents to a folder named `cornerstone_theme` in the `/docroot/themes/custom/` folder of your Drupal installation.

When installed in the correct location, the theme's `composer.json` file should be at the path: `/docroot/themes/custom/cornerstone_theme/composer.json` of your Drupal installation.
