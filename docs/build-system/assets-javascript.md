# JavaScript

> [!TIP] Be sure to read [Drupal's guide on JavaScript best practices with Drupal theming](https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview). Loading JavaScript in theme's with libraries and using attached behaviors is very important.

Theme related JavaScript should be placed in the `/src/js/` folder. Individual files should be placed in sub-folders with descriptive names so it's easy to tell at a glance what each script's purpose is.

## Example

```shell
`/src/js/object-fit-polyfill/object-fit-polyfill.init.js`
```

The theme's build system will minify all JavaScript files using [UglifyJS3](https://github.com/mishoo/UglifyJS2/tree/harmony) and copy them to the `/assets/js/` folder.
