# Gulp

You can start the development[Gulp.js](/build-system/gulp.md) build + watch task by running the `gulp` command in the root of the sub-theme directory.

> [!WARNING] If you add new files to the `/src/` directory while the gulp watch process is running, it may not process the new files. When adding new source assets it is best to _stop and restart_ the gulp watch process.

To start the build-only tasks (without the watcher task), run the `gulp build` command in the root of the sub-theme directory.

> [!TIP] If you don't want source-maps to be created with the gulp build command, you can add the `--env=prod` flag.

@todo Go into more details about Gulp build processes.
