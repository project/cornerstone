# Included Libraries

## Sass libraries

The following Sass and PostCSS libraries are installed and imported by default:

| Name | Description | Required for Sub-theme |
| --- | --- | :-: |
| [include-media](https://include-media.com) | Media query breakpoint management. | ✅ |
| [Bourbon v5](http://bourbon.io) | Sass Mixin library. | ✅ |
| [Modularscale](https://github.com/modularscale/modularscale-sass) | Modular scale mixin/function library. | ✅ |
| [sass-yiq](https://github.com/timhettler/sass-yiq) | Sass YIQ color contrast function. | ❌ |
| [font-face-generator](https://www.npmjs.com/package/font-face-generator) | Webfont @font-face Sass generator. | ❌ |
| [range-slider-sass](https://github.com/Jimdo/range-slider-sass) | A sass snippet for styling input type range. | ❌ |
| [Select Box](https://github.com/dbox/select-box) | Helper mixin for styling select form elements. | ❌ |

## PostCSS libraries

The following PostCSS plugins are installed by default:

| Folder/Filename | Description | Required for Sub-theme |
| --- | --- | :-: |
| [autoprefixer](https://github.com/postcss/autoprefixer) | Parse CSS and add vendor prefixes to rules by Can I Use. | ✅ |
| [postcss-assets](https://github.com/borodean/postcss-assets) | An asset manager for PostCSS. | ❌ |
| [postcss-easysprites](https://github.com/glebmachine/postcss-easysprites) | Easy sprites for postcss. Just append `#spritename` to the end of image url. No complicated mechanism or strict folder structure.. | ❌ |
| [postcss-flexbugs-fixes](https://github.com/luisrudge/postcss-flexbugs-fixes) | PostCSS plugin that tries to fix all of flexbug's issues. | ✅ |
| [postcss-inline-svg](https://github.com/TrySound/postcss-inline-svg) | PostCSS plugin to reference an SVG file and control its attributes with CSS syntax. | ❌ |
| [postcss-svgo](https://github.com/ben-eb/postcss-svgo) | Optimize inline SVG with PostCSS. | ❌ |
